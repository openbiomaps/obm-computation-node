#!/bin/bash

admin_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32})
htpasswd -c -b .htpasswd obm-cn-admin $admin_password 
printf "Admin password: %s\n" $admin_password
echo $admin_password > .env

#echo '
#<FilesMatch "index\.(php)">
#    AuthType Basic
#    AuthName "Admin"
#    AuthUserFile .htpasswd
#    require valid-user
#</FilesMatch>' >> .htaccess


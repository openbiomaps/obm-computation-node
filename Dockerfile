# This is a Dockerfile for creating Docker files for packages, so we need some special system library and R packages
FROM rocker/r-base:latest
WORKDIR /main

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    # This needed for .. \
        file \
    # These libs needed for devtools R package: \
        # libcurl4-openssl-dev \ 
        libcurl4-gnutls-dev \
        libxml2-dev \
        libssl-dev \
        libgit2-dev \
        libharfbuzz-dev \
        libfribidi-dev \
        libfontconfig1-dev \
    # This is needed rgdal package \
        libgdal-dev \
    # This is needed for obm-r package \
        libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# Do you might need renv ?
#RUN R -e "install.packages('renv', repos = c(CRAN = 'https://cloud.r-project.org'))"

# devtools is essential for containerit
RUN R -e "install.packages('devtools', dependencies = TRUE,  repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN R -e "library('devtools');devtools::install_github('o2r-project/containerit')"
RUN R -e "devtools::install_github('OpenBioMaps/obm.r')"

<?php

$headers = apache_request_headers();
#$client_key = isset($_POST['client-key']) ? $_POST['client-key'] : 0;
$client_key = isset($headers['client-key']) ? $headers['client-key'] : 0;
$client_name = isset($headers['client-name']) ? $headers['client-name'] : 0;

$rundir = getcwd();
$workdir = "main";

# client's service key validate against the request's REMOTE_ADDR
if (!validate_client_key($client_key,$client_name)) {
    http_response_code(403);
    die('Forbidden');
}

if (isset($_POST['package'])) {
    //echo common_message('ok','connected');

    $pkg = json_decode($_POST['config-file'],true);
    
    $obm_project = $_POST["project"];
    $package = $_POST['package'];
    $user = $_POST['user'];
    $jobdir = "$rundir/jobs/$obm_project/$package";
    mkdir($jobdir,0777,true);

    // Package Send / Send
    if (isset($_POST['method']) and $_POST['method'] == 'create') {

        $logfile = fopen("$jobdir/create.log", "w") or die("Unable to write logfile!");
        fwrite($logfile, "Creating $obm_project/$package\n");
        fclose($logfile);

        mkdir($workdir);
        mkdir("$workdir/$obm_project");
        mkdir("$workdir/$obm_project/$package");
        chdir("$workdir/$obm_project/$package");

        mkdir("$workdir/$obm_project/$package/output");

        // packed package files arrive from OBM server 
        if (isset($_FILES['files'])) {
            if (!is_array($_FILES['files']['tmp_name']))
                $s = move_uploaded_file($_FILES['files']['tmp_name'], $_FILES['files']['name']);
            else {
                for ( $i = 0; $i<count($_FILES['files']['tmp_name']); $i++ ) {
                    #$p = array_shift($pkg['includes']);
                    #$path = "";
                    #if (preg_match("/(.+)\/".$_FILES['files']['name'][$i]."$/",$p,$m)) {
                    #    $path = $m[1]."/";
                    #    mkdir($path);
                    #}
                    #move_uploaded_file($_FILES['files']['tmp_name'][$i], $path.$_FILES['files']['name'][$i]);
                    move_uploaded_file($_FILES['files']['tmp_name'][$i], $_FILES['files']['name'][$i]);
                }
            }
        }
        // extracting package content
        if (file_exists($obm_project.'_'.$package.'.tar.gz')) {

            if (file_exists($obm_project.'_'.$package.'.tar'))
                unlink($obm_project.'_'.$package.'.tar');
            try {
                $phar = new PharData("$rundir/$workdir/$obm_project/$package/$obm_project".'_'.$package.'.tar.gz'); 
                $phar->decompress();
            } catch (Exception $e) {
                // handle errors
                echo common_message("fail","Extract failed: ".$e->getMessage());

                $logfile = fopen("$jobdir/create.log", "a") or die("Unable to write logfile!");
                fwrite($logfile, "Extract failed: $e");
                fclose($logfile);

                exit;
            }

            try {
                $phar = new PharData("$rundir/$workdir/$obm_project/$package/$obm_project".'_'.$package.'.tar'); 
                $phar->extractTo("$rundir/$workdir/$obm_project/$package/",null,true);
            } catch (Exception $e) {
                // handle errors
                echo common_message("fail","Extract failed: ".$e->getMessage());

                $logfile = fopen("$jobdir/create.log", "a") or die("Unable to write logfile!");
                fwrite($logfile, "Extract failed: $e");
                fclose($logfile);
                exit;
            }
        }

        // package Docker build using obm-cn supercontainer 
        foreach ($pkg['environment'] as $envs) {
            if ($envs["type"] == 'R') {
                # Create package's docker
                if (!file_exists("$rundir/$workdir/$obm_project/$package/Dockerfile") or 
                    (isset($pkg['build_docker']) and $pkg['build_docker']==true)) {

                    link("$rundir/libs/init.R","$rundir/$workdir/$obm_project/$package/obm_init.R");

                    $process = new Process("docker run --rm -v $rundir/$workdir/$obm_project/$package:/main registry.gitlab.com/openbiomaps/obm-computation-node:latest Rscript --vanilla --no-save obm_init.R {$pkg['mainscript']}");
                    $process->logfile = "$jobdir/create.log";
                    $process->start();
                    echo common_message('success','The process call sent, check the status');
                    $process->writePid("$jobdir/create.pid");
                } else {
                    echo common_message('success','Docker file exists, if you want to recreate it, use the `build_docker: true` option in the computation_conf.yml');
                }
            }
        }

    }
    // Package Send / Refresh
    elseif (isset($_POST['method']) and $_POST['method'] == 'init') {
        // reinit package without changing uploaded files, only config used
        foreach ($pkg['environment'] as $envs) {
            if ($envs["type"] == 'R') {

                # Create package's docker
                if (!file_exists("$rundir/$workdir/$obm_project/$package/Dockerfile") or 
                    (isset($pkg['build_docker']) and $pkg['build_docker']==true)) {
                    link("$rundir/libs/init.R","$rundir/$workdir/$obm_project/$package/obm_init.R");

                    $process = new Process("docker run --rm -v $rundir/$workdir/$obm_project/$package:/main registry.gitlab.com/openbiomaps/obm-computation-node:latest Rscript --vanilla --no-save obm_init.R {$pkg['mainscript']}");
                    $process->logfile = "$jobdir/create.log";
                    $process->start();
                    echo common_message('success','The process call sent, check the status');
                    $process->writePid("$jobdir/create.pid");
                }
            }
        }
    }
    // Build package's Docker image
    elseif (isset($_POST['method']) and $_POST['method'] == 'build') {

        chdir("$rundir/$workdir/$obm_project/$package");
        $process = new Process("docker build --tag $package .");
        $process->logfile = "$jobdir/build.log";
        echo $process->start();
        echo common_message('success','The process call sent, check the status');
        $process->writePid("$jobdir/build.pid");
    }
    // Run package's docker
    elseif (isset($_POST['method']) and $_POST['method'] == 'run') {
        // The CMD command built in the Dockerfile, but the volume attached on the fly, so if you change anything  in the script files will produce new results
        // Currently there is no way to update the Dockerfile
        // We don't want to keep running computational packages
        $process = new Process("docker run --rm -v $rundir/$workdir/$obm_project/$package:/payload $package");

        $process->logfile = "$jobdir/run.log";
        $process->start();
        echo common_message('success','The process call sent, check the status');
        $process->writePid("$jobdir/run.pid");
    } 
    // Stop package's docker
    elseif (isset($_POST['method']) and $_POST['method'] == 'stop') {
        $process = new Process();
        if ($process->stop())
            echo common_message('success','The process has been stopped');
        else
            echo common_message('success','The process has not been stopped');
    }
    exit;
}

if (isset($_GET['package'])) {
    $workdir = "main";
    $package = $_GET['package'];
    $obm_project = $_GET['project'];
    chdir("$workdir/$obm_project/$package");
    $jobdir = "$rundir/jobs/$obm_project/$package";
    $y = yaml_parse_file("computation_conf.yml");

    if (!$y) {
        echo common_message("error","Syntax error in config file");

    } else  {

        if (isset($_GET['method']) and $_GET['method']=='state') {
            echo common_message('ok',get_running_state($_GET['level'],$obm_project,$package));
            exit;
        }

        elseif (isset($_GET['method']) and $_GET['method']=='results') {
            $y = yaml_parse_file("computation_conf.yml");
            
            $original_files = explode("\n",shell_exec("tar -tf ".$obm_project.'_'.$package.'.tar'));

            $file_list = array();
            $path = "$rundir/$workdir/$obm_project/$package/";
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $filename)
            {
                if (preg_match('|/\.|',$filename)) continue;
                $file_list[] = preg_replace("|$path|","",$filename);
            }
            $diff = array_diff($file_list,$original_files);

            // Load Timestamps
            $checkedFiles = array();
            if (file_exists("$jobdir/run.pid")) {
                $runTime = filemtime("$jobdir/run.pid");

                // Now Check & Build List
                foreach ($diff as $myFile) {
                    if ($myFile == $obm_project.'_'.$package.'.tar') continue;
                    elseif ($myFile == $obm_project.'_'.$package.'.tar.gz') continue;

                    $fileModifiedTime = filemtime($path."/".$myFile);
                    if ($fileModifiedTime >= $runTime)
                        $checkedFiles[] = $myFile;
                }
            } 
            echo common_message('ok',array('results_files'=>$checkedFiles));
            exit;
        
        } elseif (isset($_GET['method']) and $_GET['method']=='getfile') {
            if (isset($_GET['getfilekey'])) {
                if (file_exists(realpath($_GET['file']))) {
                    $mcache = new Memcached();
                    $mcache->addServer('localhost', 11211);

                     $key = genhash(32);
                    // project=dinpi&package=bombina&method=getfile&file=script/bombina.R&getfilekey
                    $mcache->set($_GET['project'].$_GET['package'].$_GET['method'].$_GET['file'], $key, 5);
                    echo $key;
                } else
                    echo 0;
                exit;
            }
            $file = realpath($_GET['file']);
            $mime = mime_content_type($file);
            header('Content-Description: File Transfer');
            header('Content-Type: '.$mime);
            #header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
    }

    exit;
}

if (isset($_POST['cn-build'])) {
    # Build init docker
    #
    # docker build -t obm-cn:1.0 .
    #
    #
    /***********  Use this Dockerfile to build your INIT environemnet  ************/
    $dockerfile = '
#FROM banmikk/obm-cn:1.0
FROM rocker/r-base:latest

WORKDIR /main

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    file \
    libcurl4-openssl-dev \
    libxml2-dev \
    libssl-dev \
    libgdal-dev \
    && rm -rf /var/lib/apt/lists/*

RUN R -e "install.packages(\'renv\', repos = c(CRAN = \'https://cloud.r-project.org\'))"
RUN R -e "install.packages(\'devtools\', repos = c(CRAN = \'https://cloud.r-project.org\'))"
RUN R -e "library(\'devtools\');devtools::install_github(\'o2r-project/containerit\')"
RUN R -e "devtools::install_github(\'OpenBioMaps/obm.r\')"';
     
    file_put_contents("$rundir/Dockerfile",$dockerfile);

    $jobdir = "$rundir/jobs";
    $process = new Process("docker build -t obm-cn:1.0");
    $process->logfile = "$jobdir/obm-cn.log";
    $process->start();
    echo common_message('success','The process call sent, check the status');
    $process->writePid("$jobdir/obm-cn.pid");

}

if (isset($_GET['load'])) {
    $loadavg = exec('cat /proc/loadavg | tr -d "\n"');
    $ncpu = exec('cat /proc/cpuinfo | grep processor | wc -l | tr -d "\n"');
    $mem_available = exec('free -h | awk \'{print $7}\' | tr -d "\n"');

    echo common_message("ok",array("loadavg"=>$loadavg,"ncpu"=>$ncpu,"mem_available"=>$mem_available));
    exit;

}


function validate_client_key($client_key,$client_name) {
    global $rundir;
    $key = 0;
    $text = "-";

    if (isset($_GET['filekey'])) {
        $mcache = new Memcached();
        $mcache->addServer('localhost', 11211);
        $value = $mcache->get($_GET['project'].$_GET['package'].$_GET['method'].$_GET['file']);
        
        if ($value !== false and $_GET['filekey'] == $value)
            return true;
    }
    if (file_exists("$rundir/clients.yml") and $client_key) {
        $y = yaml_parse_file("$rundir/clients.yml");
        $key = isset($y[$client_name]) ? $y[$client_name][0] : 0;
        $text = isset($y[$client_name]) ? $y[$client_name][1] : "-";
    }
    list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt($client_key,$key);

    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
    {
        if ( $decrypted_text == $text and $text != "-")
            return true;
    }

    return false;
}

function sslDecrypt ($ENCRYPTED_TEXT,$KEY) {
    $c = base64_decode($ENCRYPTED_TEXT);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $KEY, $options=OPENSSL_RAW_DATA, $iv);
    return array($hmac, $original_plaintext, $ciphertext_raw);
}
/* return standard JSON status message
 * https://labs.omniti.com/labs/jsend
 * */
function common_message ($return,$explanation='') {

    $status = 'unknown';
    $data = '';
    $message = '';
    $error = '';

    if (preg_match('/ok/i',$return)) {
        $status = "success";
        $message = '';
        $data = $explanation;
    } elseif (preg_match('/fail/i',$return)) {
        $status = "fail";
        $message = $explanation;
        #ErrorID#5e3c06f6592ba#
        if (preg_match('/#([0-9a-f]{13})#$/',$explanation,$hash)) {
            $data = "#$hash[1]";
        } else $data = '#';
    } elseif (preg_match('/error/i',$return)) {
        $status = "error";
        $message = $explanation;
        $data = '';
    } elseif (preg_match('/warning/i',$return)) {
        $status = "warning";
        $message = $explanation;
        $data = '';
    } elseif ($return) {
        $status = "success";
        $message = '';
        $data = $explanation;
    } elseif (!$return) {
        $status = "error";
        $message = $explanation;
        $data = '';
        // due to oauth error compatibility
        $error = $explanation;
    } elseif ($return == 'empty') {
        $status = "fail";
        $message = $explanation;
        if (preg_match('/#([0-9a-f]{13})#$/',$explanation,$hash)) {
            $data = "#$hash[1]";
        } else $data = '#';
    } else {
        echo json_encode(array('status'=>'fail','message'=>'invalid ajax message','data'=>$return));
    }

    if ($error!='')
        $result = array('status'=>$status,'message'=>$message,'data'=>$data,'error'=>$error);
    else
        $result = array('status'=>$status,'message'=>$message,'data'=>$data);

    return (json_encode($result));
}

function get_running_state($level,$obm_project,$package) {
    global $rundir;

    $jobdir = "$rundir/jobs/$obm_project/$package";

    if (file_exists("$jobdir/$level.pid")) {
        $pidfile = file("$jobdir/$level.pid");
        
        $pid = trim($pidfile[0]);
        if ($pid != '' and is_numeric($pid)) {
            $process = new Process();
            $process->setPid(trim($pidfile[0]));
            $data = file("$jobdir/$level.log");

            if ($process->status()) {
                return array("state"=>"The process is running","data"=>$data,"datetime"=>filemtime("$jobdir/$level.log"));
            } else{
                return array("state"=>"The process has finished","data"=>$data,"datetime"=>filemtime("$jobdir/$level.log"));
            }
        } else {
            return array("state"=>"The process is not running","data"=>"");
        }
        
    } else { 
        $list = glob("$jobdir/*.pid");
        $last_pid = '';
        if ( count($list)) {
            usort($list, create_function('$a,$b', 'return filemtime($a) - filemtime($b);'));
            $last_pid = "Last known action was: ".array_pop($list);
        }

        return array("state"=>"The process never runned","data"=>$last_pid);
    }
}
function get_package_state($project,$package) {
    global $rundir;

}

class Process{
    private $pid;
    private $command;
    public $logfile = '/dev/null';

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            //$this->runCom();
        }
    }
    private function runCom(){
        $command = 'nohup '.$this->command.' > '.$this->logfile.' 2>&1 & echo $!';
        exec($command ,$op);
        $this->pid = (int)$op[0];
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    public function status(){
        $command = 'ps -p '.$this->pid;
        exec($command,$op);
        if (!isset($op[1])) return false;
        else return true;
    }

    public function start(){
        if ($this->command != '') $this->runCom();
        else return true;
    }

    public function stop(){
        $command = 'kill '.$this->pid;
        exec($command);
        if ($this->status() == false) return true;
        else return false;
    }

    public function writePid($pidfile){
        $h = fopen("$pidfile", "w") or die("Unable to write pidfile!");
        fwrite($h, $this->getPid()."\n");
        fclose($h);
    }
}
function genhash($len=16) {
    // return with default(16) byte long random string
    $hash='';
    $base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz0123456789';
    $max=strlen($base)-1;
    mt_srand((double)microtime()*1000000);
    while (strlen($hash)<$len+1) $hash.=$base{mt_rand(0,$max)};
    return $hash;
}

?>

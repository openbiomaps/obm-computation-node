<!DOCTYPE html>
<html>
<head>
<title>OBM Computational server</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<style type="text/css">
.form-style-8{
	font-family: 'Open Sans Condensed', arial, sans;
	width: 500px;
	padding: 30px;
	background: #FFFFFF;
	margin: 50px auto;
	box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.22);
	-moz-box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.22);
	-webkit-box-shadow:  0px 0px 15px rgba(0, 0, 0, 0.22);

}
.form-style-8 h2{
	background: #4D4D4D;
	text-transform: uppercase;
	font-family: 'Open Sans Condensed', sans-serif;
	color: #797979;
	font-size: 18px;
	font-weight: 100;
	padding: 20px;
	margin: -30px -30px 30px -30px;
    font-weight:bold;
}
.form-style-8 input[type="text"],
.form-style-8 input[type="date"],
.form-style-8 input[type="datetime"],
.form-style-8 input[type="email"],
.form-style-8 input[type="number"],
.form-style-8 input[type="search"],
.form-style-8 input[type="time"],
.form-style-8 input[type="url"],
.form-style-8 input[type="password"],
.form-style-8 textarea,
.form-style-8 select 
{
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	outline: none;
	display: block;
	width: 100%;
	padding: 7px;
	border: none;
	border-bottom: 1px solid #ddd;
	background: transparent;
	margin-bottom: 10px;
	font: 16px Arial, Helvetica, sans-serif;
	height: 45px;
}
.form-style-8 textarea{
	resize:none;
	overflow: hidden;
}
.form-style-8 input[type="button"], 
.form-style-8 input[type="submit"]{
	-moz-box-shadow: inset 0px 1px 0px 0px #45D6D6;
	-webkit-box-shadow: inset 0px 1px 0px 0px #45D6D6;
	box-shadow: inset 0px 1px 0px 0px #45D6D6;
	background-color: #2CBBBB;
	border: 1px solid #27A0A0;
	display: inline-block;
	cursor: pointer;
	color: #FFFFFF;
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 14px;
	padding: 8px 18px;
	text-decoration: none;
	text-transform: uppercase;
}
.form-style-8 input[type="button"]:hover, 
.form-style-8 input[type="submit"]:hover {
	background:linear-gradient(to bottom, #34CACA 5%, #30C9C9 100%);
	background-color:#34CACA;
}
.result {
    font-weight: bold;
    color: red;
    text-align: center;
    padding: 1em;
}
</style>

</head>
<body>
<h1>OBM Computational server</h1>


<?php
$res = '';
$admin_access = (isset($_SERVER['PHP_AUTH_USER']) and $_SERVER['PHP_AUTH_USER'] == 'obm-cn-admin') ? true : false;

if (isset($_POST['Submit']) and $admin_access) {
    $errors = 0;
    if (strlen($_POST['client_name']) < 4 || strlen($_POST['client_name']) > 64) $errors++;
    elseif (strlen($_POST['client_key']) < 128 || strlen($_POST['client_key']) > 512) $errors++;
    elseif (strlen($_POST['client_text']) < 12 || strlen($_POST['client_text']) > 512) $errors++;

    if (!$errors) {
    
        #$new_client = array($_POST['client_name'] => array($_POST['client_key'],$_POST['client_text']));
        $y = yaml_parse_file("clients.yml");
        $update = 0;
        $clients = array();
        foreach($y as $key=>$client) {
            if (is_array($client)) {
                if ($key == 'name' and  $client[0] == 'key') continue;
                if ($key == $_POST['client_name']) {
                    $client[0] = $_POST['client_key'];
                    $client[1] = $_POST['client_text'];
                    $update++;
                }
                $clients[$key] = $client;
            }
        }
        if (!$update) {
            $clients[$_POST['client_name']] = array($_POST['client_key'],$_POST['client_text']);
        }


        $r = yaml_emit_file('clients.yml',$clients);
    } else {
        $r = false;
    }
    if ($r) {
        $res = "Update done.";
    } else {
        $res = "Update failed!";
    }
}

$valid_client = 0;
if (file_exists("clients.yml")) {
    $y = yaml_parse_file("clients.yml");
    foreach($y as $key=>$client) {
        if (is_array($client)) {
            if ($key == 'name' && $client[0] == 'key' && $client[1] == 'sec')
                continue;
            elseif (strlen($client[0]) > 64 && strlen($client[1])> 10) {
                $valid_client++;
            }
        }
    }
}
# New installation
if (!file_exists("clients.yml") or !$valid_client) {
    #... do something?
}

if ($admin_access) {
?>
<div class="form-style-8">
  <h2>Register a new computation client</h2>
  <form method='post'>
    <input type="text" required="required" name="client_name" placeholder="Client name" />
    <input type="text" required="required" minlength="128" name="client_key" placeholder="Client key" />
    <input type="text" required="required" minlength="12" name="client_text" placeholder="Client text" />
    <input type="submit" name="Submit" value="Submit" />
  </form>
  <div class='result'><?php echo $res ?></div>
</div>

<?php
}

?>

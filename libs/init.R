library('containerit')
# source your R code

args = commandArgs(trailingOnly=TRUE)

scriptFile <- args[1] #'script/bombina.R'
scriptCmd <- CMD_Rscript(scriptFile)

# build the environment for your code
dockerfile_obj <- containerit::dockerfile(from = scriptFile, silent = TRUE, cmd = scriptCmd, copy = list("./"))

#workspace <- tempdir()
#datafile <- file.path(workspace, "dataset.csv")
#dockerfile_obj <- containerit::dockerfile(from = workspace, cmd = cmd, image = getImageForVersion("3.3.3"), copy = "script_dir")

containerit::write(dockerfile_obj, file = 'Dockerfile')

#outfile <- paste("I'm ",basename(scriptFile),"!")
#dir.create("output", showWarnings = TRUE, recursive = FALSE, mode = "0755")
#setwd("output")
#writeLines(outfile, "output/init_output.txt")
